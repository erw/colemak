
-- Erik Ramsgaard Wognsen, 2011
-- This work is placed in the public domain.

import Data.Char
import Data.List (elemIndices, sortBy, intercalate)
import Data.Function (on)
import qualified Data.Set as Set
import Text.Printf (printf)

main = interact charfreq

barGraphWidth = 60
maxNumCharsToShow = 100

homeKeysColemak = "arstneio"
homeKeysDvorak = "aoeuhtns"
-- homeKeysQwerty = "asdfjkl;:"
homeKeysQwerty = "asdfjklæ"  -- Danish

charfreq input =
    let
        text = filter (not . isSpace) $ map toLower input
    in
        if
            null text
        then
            "Not enough input to analyze.\n"
        else
            let
                totalCount = length text
                unique = Set.toList $ Set.fromList $ text
                charCountPairs = [(char, length $ elemIndices char text) | char <- unique]
                charCountPairsSorted = reverse $ sortBy (compare `on` snd) charCountPairs
                maxCount = snd $ head charCountPairsSorted
                charCountPairsRanked = zip [1..] charCountPairsSorted
                formattedRows = map (formatRow maxCount totalCount barGraphWidth) charCountPairsRanked
            in
                unlines $ take maxNumCharsToShow formattedRows

formatRow :: Int ->   Int ->     Int ->        (Int,  (Char, Int))   -> String
formatRow    maxCount totalCount barGraphWidth (rank, (char, count)) =
    let
        barSize = barGraphWidth * count `div` maxCount
        percentage = (fromIntegral count) / (fromIntegral totalCount) * 100 :: Float
    in
        intercalate "   "
            [ printf "%6d" rank
            , [char]
            , if elem char homeKeysQwerty  then "Q" else " "
            , if elem char homeKeysDvorak  then "D" else " "
            , if elem char homeKeysColemak then "C" else " "
            , printf "%6d" count
            , printf "%5.2f" percentage ++ "%"
            , replicate barSize '#'
            ]
