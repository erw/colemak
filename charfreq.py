#!/usr/bin/env python3

# Erik Ramsgaard Wognsen, 2011
# This work is placed in the public domain.

'''
Counts and reports the frequency of characters read from stdin (ignoring case
and not counting whitespace). Annotates the output with locations of the
eight home keys for the QWERTY, Dvorak and Colemak keyboard layouts.
'''

from operator import itemgetter
import sys
import re

MAX_NUM_CHARS_TO_SHOW = 100
BAR_GRAPH_WIDTH = 60

HOME_KEYS_QWERTY  = 'asdf' 'jkl;:'
HOME_KEYS_DVORAK  = 'aoeu' 'htns'
HOME_KEYS_COLEMAK = 'arst' 'neio'
HOME_KEYS_WORKMAN = 'asht' 'neoi'
HOME_KEYS_CARPALX = 'dstn' 'aeoh'  # QGMLWB or QGMLWY
INDEX_LEFT = 'pgtdvb'
INDEX_RIGTH = 'jlhnkm'

if 1:  # Danish
    HOME_KEYS_QWERTY = HOME_KEYS_QWERTY.replace(';:', 'æ')


def main():
    text = ''.join(sys.stdin.readlines()).lower()
    text = re.sub('\s', '', text)
    char_count_pairs = [(char, text.count(char)) for char in set(text)]

    if not char_count_pairs:
        print('Not enough input to analyze.')
        sys.exit(1)

    char_count_pairs.sort(key=itemgetter(1), reverse=True)
    max_count = char_count_pairs[0][1]
    total_count = len(text)

    # Present
    print()
    for rank, (char, count) in enumerate(char_count_pairs
                                         [:MAX_NUM_CHARS_TO_SHOW]):
        print('',
              str(rank + 1).rjust(len(str(MAX_NUM_CHARS_TO_SHOW))),
              char,
              # 'L' if char in INDEX_LEFT else ' ',
              # 'R' if char in INDEX_RIGTH else ' ',
              'Q' if char in HOME_KEYS_QWERTY else ' ',
              'D' if char in HOME_KEYS_DVORAK else ' ',
              'C' if char in HOME_KEYS_COLEMAK else ' ',
              'W' if char in HOME_KEYS_WORKMAN else ' ',
              'X' if char in HOME_KEYS_CARPALX else ' ',
              str(count).rjust(len(str(max_count))),
              ('%.2f%%' % (count / total_count * 100)).rjust(6),
              '#' * int(count / max_count * BAR_GRAPH_WIDTH),
              sep='   ')
    print()

if __name__ == '__main__':
    main()
