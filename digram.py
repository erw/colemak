#!/usr/bin/env python3

# Erik Ramsgaard Wognsen, 2011
# This work is placed in the public domain.

'''
Counts and reports digrams in text read from stdin where digram is simply
understood as one letter/punctuation symbol followed by another. Case is
ignored and so is whitespace.

The output consists of a table of counts of the digrams present in the text
and then a list of them sorted by count. Unless the text is small, the output
will probably be many hundreds of lines.
'''

# Vim highlight same-hand row jumping:
# QWERTY:  /[zxcvb][qwert]\|[qwert][zxcvb]\|[nm][yuiop]\|[yuiop][nm]
# Colemak: /[zxcvb][qwfpg]\|[qwfpg][zxcvb]\|[km][jluyæ]\|[jluyæ][km]

from operator import itemgetter, eq
import sys
import signal

signal.signal(signal.SIGPIPE, signal.SIG_DFL)  # Handle piping

# Digrams that are typed with the same finger are highlighted. However, same
# *letter* digrams cannot be avoided on any conventional keyboard layout. You
# can choose whether you want them included in the analysis.
INCLUDE_SAME_LETTER = False


ALPHABET = list('abcdefghijklmnopqrstuvwxyz')
FINGERS_COLEMAK = ['qaz', 'wrx', 'fsc', 'pgtdvb', 'jlhnkm', 'ue', 'yi', 'o']
FINGERS_DVORAK = ['a', 'oq', 'ej', 'pyuikx', 'fgdhbm', 'ctw', 'rnv', 'lsz']
FINGERS_QWERTY = ['qaz', 'wsx', 'edc', 'rtfgvb', 'yuhjnm', 'ik', 'ol', 'p']

if 1:  # Danish
    ALPHABET += 'æøå'
    FINGERS_COLEMAK[7] += 'æøå'
    FINGERS_DVORAK[0] += 'æøå'
    FINGERS_QWERTY[7] += 'æøå'

if 1:  # Punctuation
    ALPHABET += ",.'"
    FINGERS_COLEMAK[5] += ","
    FINGERS_COLEMAK[6] += "."
    FINGERS_COLEMAK[7] += "'"
    FINGERS_DVORAK[0] += "'"
    FINGERS_DVORAK[1] += ","
    FINGERS_DVORAK[2] += "."
    FINGERS_QWERTY[5] += ","
    FINGERS_QWERTY[6] += "."
    FINGERS_QWERTY[7] += "'"

if not (sorted(ALPHABET) ==
        sorted(''.join(FINGERS_COLEMAK)) ==
        sorted(''.join(FINGERS_DVORAK)) ==
        sorted(''.join(FINGERS_QWERTY))):
    print('Alphabet/finger mismatch')
    sys.exit(1)

# Process
text = ''.join(sys.stdin.readlines()).lower()
dig_count_map = {(letter0 + letter1): text.count(letter0 + letter1)
                 for letter0 in ALPHABET
                 for letter1 in ALPHABET}

max_count = max(dig_count_map.values())
count_width = len(str(max_count)) + 1
rank_width = len(str(len(ALPHABET) ** 2))
threshold = 0
# nonzero_counts = [v for v in dig_count_map.values() if v > 0]
# avg_nonzero_count = sum(nonzero_counts) / len(nonzero_counts)
# threshold = avg_nonzero_count / 4


# Print matrix
print()
print('↓ First char \ second char →')
for letter1 in [' '] + ALPHABET:
    print(letter1, end='')  # row names
    for letter2 in ALPHABET:
        if letter1 == ' ':
            out = letter2  # column names
        else:
            n = dig_count_map[letter1 + letter2]
            if letter1 == letter2:
                out = '*'
                if INCLUDE_SAME_LETTER and n > threshold:
                    out = n
            else:
                out = n if n > threshold else ''
        print(str(out).rjust(count_width), end='')
    print()
print()


# Print list
print()
dig_count_sorted = [item
                    for item
                    in dig_count_map.items()
                    if ((INCLUDE_SAME_LETTER or not eq(*item[0])) and
                        item[1] > threshold)]
dig_count_sorted.sort()  # on digram
dig_count_sorted.sort(key=itemgetter(1), reverse=True)  # on count


def get_finger_indices(dig, fingers):
    return [index
            for letter in dig
            for index, finger in enumerate(fingers)
            if letter in finger]

BAR_GRAPH_WIDTH = 60

same_finger_colemak = same_finger_dvorak = same_finger_qwerty = 0
total_count = 0

for rank, (dig, count) in enumerate(dig_count_sorted):
    indices_colemak = get_finger_indices(dig, FINGERS_COLEMAK)
    indices_dvorak = get_finger_indices(dig, FINGERS_DVORAK)
    indices_qwerty = get_finger_indices(dig, FINGERS_QWERTY)
    if eq(*indices_colemak):
        same_finger_colemak += count
    if eq(*indices_dvorak):
        same_finger_dvorak += count
    if eq(*indices_qwerty):
        same_finger_qwerty += count
    total_count += count

    # handing = ''.join('L' if index in range(4) else 'R'
    #                   for index in indices_colemak)

    print('',
          str(rank + 1).rjust(rank_width),
          dig,
          # '   ' * handing.count('R') + handing + '   ' * handing.count('L'),
          '=C=' if eq(*indices_colemak) else '   ',
          '=D=' if eq(*indices_dvorak) else '   ',
          '=Q=' if eq(*indices_qwerty) else '   ',
          str(count).rjust(count_width),
          '#' * int(count / max_count * BAR_GRAPH_WIDTH),
          sep='   ')

print(' ' * (rank_width + 8),
      str(same_finger_colemak).rjust(5),
      str(same_finger_dvorak).rjust(5),
      str(same_finger_qwerty).rjust(5),
      str(total_count).rjust(7),
      )
print(' ' * (rank_width + 8),
      str(round(same_finger_colemak / total_count * 100, 2)).rjust(5),
      str(round(same_finger_dvorak / total_count * 100, 2)).rjust(5),
      str(round(same_finger_qwerty / total_count * 100, 2)).rjust(5),
      'percent',
      )
