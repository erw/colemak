// based on a keyboard map from an 'xkb/symbols/dk' file

partial default alphanumeric_keys
xkb_symbols "basic" {

    include "latin(type2)"

    name[Group1]="Danish";

    key <AE11>	{ [      plus,   question,    plusminus, questiondown ]	};
    key <AE12>	{ [dead_acute, dead_grave,          bar,    brokenbar ]	};


    key <AC10>	{ [        ae,        AE,   dead_acute, dead_doubleacute ] };
    key <AC11>	{ [    oslash,  Ooblique, dead_circumflex, dead_caron ]	};
    key <TLDE>	{ [   onehalf,   section, threequarters,    paragraph ]	};

    key <BKSL>	{ [apostrophe,   asterisk, dead_doubleacute, multiply ]	};

    key <LSGT>	{ [      less,    greater,    backslash,      notsign ]	};

    include "kpdl(comma)"

    include "level3(ralt_switch)"
};

partial alphanumeric_keys
xkb_symbols "nodeadkeys" {

    include "dk(basic)"

    name[Group1]="Danish (eliminate dead keys)";

    key <AE12>	{ [     acute,      grave,          bar,       ogonek ]	};
    key <AD11>	{ [     aring,      Aring,    diaeresis,       degree ]	};
    key <AD12>	{ [ diaeresis, asciicircum,  asciitilde,       macron ]	};
    key <AC10>	{ [        ae,         AE,        acute,  doubleacute ]	};
    key <AC11>	{ [    oslash,   Ooblique,  asciicircum,        caron ]	};
    key <BKSL>	{ [apostrophe,   asterisk,  doubleacute,     multiply ]	};
    key <AB08>	{ [     comma,  semicolon,      cedilla,       ogonek ]	};
    key <AB09>	{ [    period,      colon, periodcentered,   abovedot ]	};

};

// Copied from macintosh_vndr/dk
partial alphanumeric_keys 
xkb_symbols "mac" {

    include "dk"
    name[Group1]= "Danish (Macintosh)";

    key <SPCE>	{ [    space,       space, nobreakspace, nobreakspace ]	};
    key <AB10>	{ [    minus,  underscore,       hyphen,       macron ]	};
    include "kpdl(dot)"
};


partial alphanumeric_keys 
xkb_symbols "mac_nodeadkeys" {
    include "dk(mac)"
    name[Group1]= "Danish (Macintosh, eliminate dead keys)";

    key <AE12>	{ [    acute,       grave,          bar,       ogonek ]	};
    key <AD12>	{ [diaeresis, asciicircum,   asciitilde,  dead_macron ]	};
};

partial alphanumeric_keys 
xkb_symbols "dvorak" {
    include "no(dvorak)"

    name[Group1]= "Danish (Dvorak)";
    key <AD12> { [  asciitilde, asciicircum, dead_diaeresis, dead_tilde ] };
};




// Colemak symbols for xkb on X.Org Server 7.x
// 2006-01-01 Shai Coleman, http://colemak.com/ . Public domain.

// 2011-09-09 Modified for Danish by erw

// 2013-01-24 Personalized by erw for erw.
// Some changes over standard Colemak are made to ease:
// - use on a Kinesis Advantage keyboard
// - programming
// - typing Lithuanian 

partial alphanumeric_keys
xkb_symbols "colemak" {
    include "dk"
    name[Group1]= "Danish (Colemak)";

    // Alphanumeric section

    //  <AE01> unmodified
    //  <AE02> unmodified
    //  <AE03> unmodified
    key <AE04> { [            4,       dollar,       eabovedot,        Eabovedot ] };  // dollar instead of currency, eabovedot (for Lithuanian) instead of dollar/onequarter
    key <AE05> { [            5,      percent,        EuroSign,             cent ] };  // EuroSign instead of onehalf
    //  <AE06> unmodified
    //  <AE07> unmodified
    //  <AE08> unmodified
    //  <AE09> unmodified
    //  <AE10> unmodified
    key <AE11> { [         plus,     question,    questiondown,        plusminus ] };  // questiondown and plusminus swapped for consistency with '¡' and '!'
    key <AE12> { [       Escape,       Escape,          Escape,           Escape ] };  // An extra Esc key for the Kinesis Advantage (dead_grave and dead_acute are on R and T instead)

    key <AD01> { [            q,            Q,      adiaeresis,       Adiaeresis ] };
    key <AD02> { [            w,            W,           aring,            Aring ] };
    key <AD03> { [            f,            F,          atilde,           Atilde ] };
    key <AD04> { [            p,            P,          oslash,         Ooblique ] };
    key <AD05> { [            g,            G,     dead_ogonek,       asciitilde ] };
    key <AD06> { [            j,            J,         umacron,          Umacron ] };  // umacron (for Lithuanian) instead of dstroke
    key <AD07> { [            l,            L,         lstroke,          Lstroke ] };
    key <AD08> { [            u,            U,          uacute,           Uacute ] };
    key <AD09> { [            y,            Y,      udiaeresis,       Udiaeresis ] };
    key <AD10> { [           ae,           AE,      odiaeresis,       Odiaeresis ] };
    //  <AD11> unmodified
    key <AD12> { [   asciitilde,  asciicircum,      dead_tilde,  dead_circumflex ] };  // Kinesis Advantage special location

    key <AC01> { [            a,            A,          aacute,           Aacute ] };
    key <AC02> { [            r,            R,      dead_grave,       asciitilde ] };
    key <AC03> { [            s,            S,          scaron,           Scaron ] };  // scaron (for Lithuanian) instead of ssharp
    key <AC04> { [            t,            T,      dead_acute, dead_doubleacute ] };
    key <AC05> { [            d,            D,  dead_diaeresis,       asciitilde ] };
    key <AC06> { [            h,            H,      dead_caron,       asciitilde ] };


    key <AC07> { [            n,            N,          ntilde,           Ntilde ] };
    key <AC08> { [            e,            E,          eacute,           Eacute ] };
    key <AC09> { [            i,            I,          iacute,           Iacute ] };
    key <AC10> { [            o,            O,          oacute,           Oacute ] };
    //  <AC11> unmodified

    key <AB01> { [            z,            Z,          zcaron,           Zcaron ] };  // zcaron (for Lithuanian) instead of ae/AE
    key <AB02> { [            x,            X, dead_circumflex,       asciitilde ] };
    key <AB03> { [            c,            C,          ccaron,           Ccaron ] };  // ccaron (for Lithuanian) instead of ccedilla
    key <AB04> { [            v,            V,              oe,               OE ] };
    key <AB05> { [            b,            B,      dead_breve,       asciitilde ] };
    key <AB06> { [            k,            K,  dead_abovering,       asciitilde ] };
    key <AB07> { [            m,            M,              mu,      dead_macron ] };  // mu inserted, dead_macron moved to shift altgraphed
    key <AB08> { [        comma,    semicolon,     dead_ogonek,     dead_cedilla ] };  // swap cedilla and ogonek to make Lithuanian easier
    //  <AB09> unmodified
    key <AB10> { [        minus,   underscore,            Menu,    dead_abovedot ] };  // Menu instead of dead_belowdot

    // key <CAPS> { [    BackSpace,    BackSpace,       BackSpace,        BackSpace ] };
    key <SPCE> { [        space,        space,           space,     nobreakspace ] };

    key <LSGT> { [         less,      greater,         onehalf,          section ] };  // Original dk <TLDE> un-altgraphed symbols (onehalf, section) moved to altgraphed <LSGT>
    key <TLDE> { [    backslash,          bar,       backslash,              bar ] };  // Kinesis Advantage special location
    key <BKSL> { [   apostrophe,     asterisk,           grave,            U2212 ] };  // grave instead of dead_doubleacute, minus sign (U2212) instead of multiply

    include "level3(ralt_switch)"
};
